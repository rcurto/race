(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('feedbackController', feedbackListController);

    feedbackListController.$inject = ['$scope', 'feedbackFactory', 'atletaCursaFactory','userFactory','feedbackStandardAnswerFactory','eventFactory','$filter'];
    function feedbackListController($scope, feedbackFactory, atletaCursaFactory,userFactory,feedbackStandardAnswerFactory,eventFactory,$filter) {

        $scope.env = {
            loading: true,
            feedback: [],
            races: [],
            raceAnswers: [],
            weeklyAnswers: [],
            athletes:[],
            filters:{
                //username: undefined
            }
        };
        $scope.$parent.initPage = init; // start point

        var promises = [];
        $scope.user = null;

        function init() {
            var user = $scope.$parent.user;
            $scope.user = user;

            if (user.isAthlete()) {
                var promiseFeedback = user.getFeedback().then(function (feedback) {
                    $scope.env.feedback = feedback;
                });
                promises.push(promiseFeedback);


            } else {
                var promiseFeedback = feedbackFactory.getFeedbackWitOutCoachComment().then(function (feedbacks) {
                    $scope.env.feedback = feedbacks;

                });
                promises.push(promiseFeedback);

                var raceFeedback = atletaCursaFactory.getRaceWitOutCoachComment().then(function (races) {
                    $scope.env.races = races;
                });
                promises.push(raceFeedback);

                var athletesPromise = userFactory.getAllUsers().then(function (response) {
                    $scope.env.athletes = response;
                });
                promises.push(athletesPromise);


                var weeklyAnswersPromise = feedbackStandardAnswerFactory.getAllByType('weekly').then(function (response) {
                    $scope.env.weeklyAnswers = response;
                });
                promises.push(weeklyAnswersPromise);

                var raceAnswersPromise = feedbackStandardAnswerFactory.getAllByType('race').then(function (response) {
                    $scope.env.raceAnswers = response;
                });
                promises.push(raceAnswersPromise);
            }



            Parse.Promise.when(promises).then(function () {
                angular.forEach($scope.env.feedback, function (row) {
                    if (row.get('fullFeedback') != undefined) {
                        return;
                    }
                    if (moment().isAfter(moment(row.get('startDate')).add(3, 'days'))) {
                        $scope.env.feedbackAlter = true;
                    }
                });
                $scope.$apply();
                pageLoaded();
            });
        }

        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply();
        }

        $scope.saveWeeklyFeedback = function(feedback){
            console.log(feedback.isCoachFull )

            if ( !feedback.coachFeedback ){
                eventFactory.createCoachFeedback(feedback.user)
            }
            feedback.editComment=undefined;
            feedback.full(feedback, $scope.user.isCoach() );

            alertify.success($filter('translate')('Weekly feedback saved'));

        };

        $scope.saveCoachCommentsResult = function(post, comment, default_comment){
            //if ( !post.isCoachFull ){

            eventFactory.createRaceFeedback(post.get('user'), post.get('nomcursa'))
            // }
            post.update({coachComments:comment,isCoachFull: true, flgUserView: false,defaultCoachComment: default_comment},function(){
                alertify.success($filter('translate')('Race feedback saved'));

                post.isCoachFull = true;


            })

        }

    }
})(angular);