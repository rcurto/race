(function (angular) {
    'use strict';
    angular.module('core').service('cursesFactory', cursesFactory);

    cursesFactory.$inject = ['cursesService', 'atletaCursaFactory'];

    function cursesFactory(cursesService, atletaCursaFactory) {
        var cache = [];
        return {
            getObjectByCategory: getObjectByCategory,
            getById: getById,
            getAll: getAll,
            getNextPrincipalRace: getNextPrincipalRace,
            getNextSecondaryRace: getNextSecondaryRace,
            getCount: getCount,
            store: store,
            getRaceWithOutJoin  : getRaceWithOutJoin,
            getUpcomingRaces    : getUpcomingRaces,
            topRaces:topRaces
        };

        function getNextPrincipalRace(user) {
            var promise = new Parse.Promise();
            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);
            query.equalTo('objective', 'principal');
            query.include('nomcursa');
            query.include('nomcursa.country');
            var races = [];
            query.find().then(function(result){
                for(var i in result){
                    if ( result[i].get('nomcursa') )
                    races.push(result[i].get('nomcursa').id)
                }
                var Curses = new Parse.Query(Parse.Object.extend("Curses"));
                Curses.greaterThan('data', new Date());
                Curses.containedIn('objectId', races);
                Curses.ascending('data');
                Curses.first().then(function(result){
                    result = result ? cursesService(result) : null;
                    promise.resolve(result);
                })
            });

            return promise;
        }

        function getNextSecondaryRace(user) {

            var promise = new Parse.Promise();
            var parseObject = Parse.Object.extend("atleta_cursa");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);
            query.equalTo('objective', 'secondary');
            query.include('nomcursa');
            query.include('nomcursa.country');
            var races = [];
            query.find().then(function(result){
                for(var i in result){
                    races.push(result[i].get('nomcursa').id)
                }
                var Curses = new Parse.Query(Parse.Object.extend("Curses"));
                Curses.greaterThan('data', new Date());
                Curses.containedIn('objectId', races);
                Curses.ascending('data');
                Curses.first().then(function(result){
                    result = result ? cursesService(result) : null;
                    promise.resolve(result);
                })
            });
            return promise;

        }


        function getObjectByCategory(id){
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("Curses");
            var query = new Parse.Query(parseObject);
            query.equalTo('circuit',{'objectId':id, "__type": "Pointer", "className": "Circuits"});
            query.greaterThan('data', moment().toDate() );
            query.include('zone');
            query.ascending("data");
            query.limit(1);
            query.first().then(function(result){
                {
                    result = !result ? null : cursesService(result);
                    promise.resolve(result);
                }
            });
            return promise;
        }

        function topRaces(count, callback){
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("Curses");
            var query = new Parse.Query(parseObject);
            query.descending('points');
            query.include('zone');

            query.limit(count);
            query.find({
                success: function (results) {
                    var events = [];
                    angular.forEach(results, function (row) {
                        var eventObject = cursesService(row);
                        events.push(eventObject);
                    });
                    if (callback){
                        callback(events);
                    }
                    promise.resolve(events);

                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            })
            return promise;

        }
        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/21
         # Description   : Get upcoming next races for atlete.
         ****/
        function getUpcomingRaces(count, user, callback) {
            atletaCursaFactory.getIdsByAthlete(user, function(raceIds){
                var now = moment().toDate();
                var parseObject = Parse.Object.extend("Curses");
                var query = new Parse.Query(parseObject);

                query.notContainedIn("objectId",raceIds);
                query.greaterThan('data',now);

                query.include('joined');

                query.include('zone');

                query.ascending('data');

//                query.limit(count);
                query.find({
                    success: function (results) {
                        var events = [];
                        var pIndex = 0;
                        angular.forEach(results, function (row) {

                            if(row.get("joined") != undefined){
                                if(pIndex < count){
                                    var eventObject = cursesService(row);
                                    events.push(eventObject); 
                                    pIndex++;
                                }
                            }
                            
                        });
                        callback(events);
                    },
                    error: function (error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                })
            });
        }
        function getRaceWithOutJoin(count, user, callback) {
            atletaCursaFactory.getIdsByAthlete(user, function(raceIds){
                var now = moment().toDate();
                var parseObject = Parse.Object.extend("Curses");
                var query = new Parse.Query(parseObject);
                query.notContainedIn("objectId",raceIds);
                query.greaterThan('data',now);

                query.include('joined');
                query.include('zone');

                query.ascending('data');

                query.limit(count);
                query.find({
                    success: function (results) {
                        var events = [];
                        angular.forEach(results, function (row) {
                            var eventObject = cursesService(row);
                            events.push(eventObject);
                        });
                        callback(events);
                    },
                    error: function (error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                })
            });
        }


        function getCount(callback) {
            var parseObject = Parse.Object.extend("Curses");
            var query = new Parse.Query(parseObject);
            query.count({
                success: function (count) {
                    callback(count);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function store(data, callback) {
            var parseObject = Parse.Object.extend("Curses");
            var store = new parseObject();
            var i;
            for (i in data) {
                if (i == 'parseObject') {
                    continue;
                }
                store.set(i, data[i]);
            }
            store.save(null, {
                success: function (obj) {
                    callback({success: true});
                },
                error: function (obj, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getById(id, callback) {
            var promise = new Parse.Promise();
            var parseObject = Parse.Object.extend("Curses");
            var query = new Parse.Query(parseObject);
            query.include('country');
            query.include('circuit');
            query.include('joined');
            query.include('zone');

            query.get(id, {
                success: function (results) {
                    if (callback)
                    callback(cursesService(results))
                    promise.resolve(cursesService(results));

                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;
        }


        function getAll(callback) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("Curses");
            var query = new Parse.Query(parseObject);

            var start = moment([2016, 0, 1]) ;
            var end = moment([2016, 11, 31]) ;

            query.greaterThan('data', start.toDate());
            query.lessThan('data', end.toDate());

            query.include('circuit');
            query.include('country');
            query.include('joined');
            query.include('zone');

            query.limit(1000);
            query.find({
                success: function (results) {
                    var objs = [];
                    angular.forEach(results, function (row) {
                        var eventObject = cursesService(row);
                        objs.push(eventObject);
                    });
                    if (callback){
                        callback(objs);
                    }
                    promise.resolve(objs);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;
        }


    }
})(angular);

