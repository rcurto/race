(function (angular) {
    'use strict';
    angular.module('mainApp', ['ui.router', 'core', 'angular-table', 'ui.bootstrap.datetimepicker', 'gettext']).
    config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/races');

        $stateProvider.state('login', {
            url: '/login',
            templateUrl: 'apps/mainApp/sign-in/sign-in.html',
            controller: 'signInController',
            if_user: 'index'
        }).state('index', {
            url: '/',
            templateUrl: 'apps/mainApp/curses/cursesList.html',
            controller: 'cursesListController',
            if_guest: 'login'
        }).state('race', {
            url: '/races',
            templateUrl: 'apps/mainApp/curses/cursesList.html',
            controller: 'cursesListController',
            if_guest: 'login'
        }).state('race-add', {
            url: '/races/add',
            templateUrl: 'apps/mainApp/curses/cursesForm.html',
            controller: 'cursesFormController',
            if_guest: 'login'
        }).state('race-edit', {
            url: '/races/edit/:id',
            templateUrl: 'apps/mainApp/curses/cursesForm.html',
            controller: 'cursesFormController',
            if_guest: 'login'
        }).state('race-my', {
            url: '/races/my',
            templateUrl: 'apps/mainApp/curses/myCursesList.html',
            controller: 'myCursesListController',
            if_guest: 'login'
        }).state('race-athletes', {
            url: '/races/athletes',
            templateUrl: 'apps/mainApp/curses/athletesCursesList.html',
            controller: 'myCursesListController',
            if_guest: 'login'
        })

    }).
    run(['userFactory', '$state', '$rootScope', 'gettextCatalog', function (userFactory, $state, $rootScope, gettextCatalog) {
        //gettextCatalog.currentLanguage = 'french';

        gettextCatalog.debug = true;
        var currencyUser = null;
        $rootScope.$watch(function () {
            return $state.current
        }, function (value) {
            if (value == undefined) {
                return;
            }
            userFactory.auth(function (result) {
                currencyUser = result;
                if ( currencyUser!= null ){
                    gettextCatalog.setCurrentLanguage(currencyUser.language);
                }else{
                    gettextCatalog.setCurrentLanguage('english');
                }
                if (value['if_guest'] != undefined && currencyUser == null) {
                    $state.go(value['if_guest']);
                }
                if (value['if_user'] != undefined && currencyUser != null) {
                    $state.go(value['if_user']);
                }

                /*if (value['access'] != undefined && currencyUser != null && !currencyUser.hasAccess(value['access'])) {
                    $state.go('index')
                }*/
            })
        }, true);

    }])

})(angular);


