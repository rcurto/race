(function (angular) {
    'use strict';

    angular.module('loginApp').
        controller('signInController', signInController);

    signInController.$inject = ['$scope','userFactory','$rootScope','$location'];
    function signInController($scope, userFactory, $rootScope, $location) {
        $scope.env = {
            error:null,
            sending:false,
            loading:true
        };
        $scope.actions = {
            login:login
        };
        userFactory.auth( function(user){
            $scope.env.loading = false;
            if ( user!=null ){
                window.location = 'app.html';
                $scope.$apply();
            }
        });


        function login(username, password){
            $scope.env.sending = true;
            userFactory.login(username, password,function(answer){
                if ( answer.success == false ){
                    $scope.env.error = answer.error;
                }else{
                    $rootScope.$broadcast('login');
                    window.location = 'app.html';
                }
                $scope.env.sending = false;
                $scope.$apply();
            })
        }


    }
})(angular);