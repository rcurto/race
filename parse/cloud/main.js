Parse.Cloud.afterSave("atleta_cursa", function (request, response) {
    Parse.Cloud.useMasterKey();

    var atletaCursa = request.object;
    rematchRaceRating(atletaCursa, response);


});

function rematchRaceRating(atletaCursa){
    var race = atletaCursa.get('nomcursa');

    var Feedback = Parse.Object.extend("atleta_cursa");
    var query = new Parse.Query( Feedback );
    query.equalTo('nomcursa', race );
    query.find().then(function(response){
        var total = 0;
        for( var i in response ){
            if ( response[i].get('rating')!=undefined ){
                total+=response[i].get('rating');
            }
        }

        var Race = Parse.Object.extend("Curses");
        var query = new Parse.Query( Race );
        query.get(race.id, {success: function(race){
            race.set('points', total);
            race.save();
        }})
    })
}

Parse.Cloud.define("userEdit", function (request, response) {
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Parse.User);
    query.get(request.params.id, {
        success: function (user) {
            for (var i in request.params.data) {
                user.set(i, request.params.data[i]);
                user.save(null, {
                    success: function (userObj) {
                        response.success({success: true});
                    },
                    error: function (myObject, error) {
                        response.error(error);
                    }
                });
            }
        },
        error: function (myObject, error) {
            response.error('User not found');
        }
    });
});

Parse.Cloud.define("userDelete", function (request, response) {
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Parse.User);
    query.get(request.params.id, {
        success: function (user) {
            user.destroy({
                success: function (myObject) {
                    response.success({success: true});
                },
                error: function (myObject, error) {
                    response.error(error);
                }
            })
        }, error: function () {
            response.error('User ' + request.params.id + ' not found');
        }
    });
});

Parse.Cloud.job("createFeedback", function (request, response) {
    var moment = require('cloud/moment');
    var startWeek = moment().startOf('isoWeek');
    var endWeek = moment().startOf('isoWeek').add(6,'days');

    getAthletes( function(users){
        var number = 0;
        for( var i in users ){
            addFeedback(users[i], startWeek, endWeek, function(){
                number++;
                if ( number==users.length ){
                    response.success('success');
                }
            });

        }
    })
});


Parse.Cloud.define("training", function (request, response) {
    //Parse.Cloud.useMasterKey();
    var moment = require('cloud/moment');
    var start = moment();
    var parseObject = Parse.Object.extend("atleta_mesocicle");


    var query = new Parse.Query(Parse.User);
    query.equalTo('role', {'objectId':'8MY8aPLY21',"__type": "Pointer", "className": "_Role"});
    query.find({
        success: function (users) {
            for( var i in users ){
                var user = users[i];

                var query = new Parse.Query(parseObject);
                query.equalTo('user', user);
                query.greaterThanOrEqualTo('datini', start.toDate() );
                query.lessThanOrEqualTo('datfi', start.toDate() );
                query.count({success: function(count){
                    console.log(count);
                    //response.success('finish');

                }})
                //response.success('finish');
            }
        }, error: function () {
            response.error('User ' + request.params.id + ' not found');
        }
    });
});


function addFeedback(user, startWeek, endWeek, callback){
    var Feedback = Parse.Object.extend("Feedback");

    var query = new Parse.Query( Feedback );
    query.equalTo('startDate', startWeek.toDate() );

    query.equalTo('user', user );
    query.count(function(count){
        if ( count==0 ){
            var feedback = new Feedback();
            feedback.set("user", user);
            feedback.set("startDate", startWeek.toDate());
            feedback.set("endDate", endWeek.toDate());
            feedback.save(null, {
                success: function() {
                    callback()
                }
            });
        }else{
            callback()
        }
    })
}





function getAthletes(callback){
    var query = new Parse.Query( Parse.Role );
    query.get('8MY8aPLY21',{
        success: function(role){
            var query = new Parse.Query( Parse.User );
            query.equalTo('role',role);
            query.limit(1000)
            query.find({
                success: function (users) {
                    callback(users)
                }, error: function () {
                    response.error('User ' + request.params.id + ' not found');
                }
            });
        }
    })
}