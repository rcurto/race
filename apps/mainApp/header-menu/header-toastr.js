/**
/*
ClassName	:	UIPendingNotification
Maker		:	Add by kazuyou@
Date		:	2016/03/13
Description :  Making and Control Pending Notification about athletes user.
*/
var HeaderUIToastr = function () {

    return {
        //main function to initiate the module
        init: function () {

            var $toastlast;
			var strPedingNoticeIdnex = "#";

            $('.showtaost').each(function (index) {
				$(this).click(function () {

					var shortCutFunction = 'success';
					var msg = $('#message').val();
					var title = $('#title').val() || '';
					
					toastr.options = {
						closeButton: false,
						debug: false,
						positionClass: "toast-top-right",
						onclick: null,
						showDuration	: 1000,
						hideDuration	: 1000,
						timeOut			: 500000,
						extendedTimeOut	: 1000000,
						showEasing		: 'swing',
						hideEasing		: 'linear',
						showMethod		: 'fadeIn',
						hideMethod		: 'fadeOut',
						objectId		: null,
						tableName		: null,
						multiShow		: null
					};

					msg				= $(this).attr('message');
					title			= $(this).attr('noticeTitle');
					var objectId	= $(this).attr('objectId');
					var tableName	= $(this).attr('tableName');
					var multiShow	= $(this).attr('multiShow');
					var showMsg		= $(this).attr('showMsg');
					var removable	= $(this).attr('removable');

					toastr.options.objectId		= objectId;
					toastr.options.tableName	= tableName;
					toastr.options.multiShow	= multiShow;

					totalPendingNotifCount = $('.top-menu .dropdown-notification').find(".dropdown-toggle > span").text();

					toastr.options.onclick = function () {

						if(this.multiShow == 1){
							$("[objectid='"+this.objectId+"']").parents('li.liPendgingNotification').remove();
							
							if(!isNaN(totalPendingNotifCount) && (totalPendingNotifCount - 1) < 1){
								$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-warning").removeClass("badge-default");
								totalPendingNotifCount = 0;
							}else{
								$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-default").removeClass("badge-warning");
								totalPendingNotifCount--;
							}

							$(".notificaionNumber").text(totalPendingNotifCount + " pending");
							$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").text(totalPendingNotifCount);

							//angular.element(document.getElementById('quicksidebarController')).scope().updateAthletePendingNotice(this.objectId, this.tableName);
						}
                    }

                    if(multiShow == 1 && showMsg == 'false' && removable == 'true'){

						$(this).remove();

						if(!isNaN(totalPendingNotifCount) && (totalPendingNotifCount - 1) < 1){
							$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-warning").removeClass("badge-default");
							totalPendingNotifCount = 0;
						}else{
							$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-default").removeClass("badge-warning");
							totalPendingNotifCount--;
						}

						$(".notificaionNumber").text(totalPendingNotifCount + " pending");
						$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").text(totalPendingNotifCount);

						angular.element(document.getElementById('quicksidebarController')).scope().updateAthletePendingNotice(objectId, tableName);
					}

					$("#toastrOptions").text("Command: toastr[" + shortCutFunction + "](\"" + msg + (title ? "\", \"" + title : '') + "\")\n\ntoastr.options = " + JSON.stringify(toastr.options, null, 2));

					if(showMsg == 'true' && strPedingNoticeIdnex.indexOf(index + "#") < 1 || multiShow == 2){
						var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
						$toastlast = $toast;
					}
					if(strPedingNoticeIdnex.indexOf(index + "#") < 1){
						
						strPedingNoticeIdnex = strPedingNoticeIdnex + index + "#";
					}
					
				});
			});
        }

    };

}();