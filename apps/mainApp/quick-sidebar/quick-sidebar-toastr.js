/**
/*
ClassName	:	UIPendingNotification
Maker		:	Add by kazuyou@
Date		:	2016/03/13
Description :  Making and Control Pending Notification about athletes user.
*/
var UIToastr = function () {

    return {
        //main function to initiate the module
        init: function () {

            var $toastlast;
			var strPedingNoticeIdnex = "#";

            $('.showtoast').each(function (index) {
				$(this).click(function () {
console.log("ppppppppppppppppppppppp");
					var shortCutFunction = 'success';
					var msg = $('#message').val();
					var title = $('#title').val() || '';
					
					toastr.options = {
						closeButton: false,
						debug: false,
						positionClass: "toast-top-right",
						onclick: null,
						showDuration	: 1000,
						hideDuration	: 1000,
						timeOut			: 500000,
						extendedTimeOut	: 1000000,
						showEasing		: 'swing',
						hideEasing		: 'linear',
						showMethod		: 'fadeIn',
						hideMethod		: 'fadeOut',
						objectId		: null,
						tableName		: null,
						multiShow		: null
					};

					msg				= $(this).attr('message');
					title			= $(this).attr('noticeTitle');
					var objectId	= $(this).attr('objectId');
					var tableName	= $(this).attr('tableName');
					var multiShow	= $(this).attr('multiShow');

					toastr.options.objectId		= objectId;
					toastr.options.tableName	= tableName;
					toastr.options.multiShow	= multiShow;

					toastr.options.onclick = function () {
						var totalPendingNotifCount = $('.top-menu .dropdown-notification').find(".dropdown-toggle > span").text();

						if(this.multiShow == 1){
							$("[objectid='"+this.objectId+"']").parents('li.ng-scope').remove();
							
							//$("[objectid='"+this.objectId+"]").prev('li.ng-scope').remove();

							if(!isNaN(totalPendingNotifCount) && (totalPendingNotifCount - 1) < 1){
								$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-warning").removeClass("badge-default");
								totalPendingNotifCount = 0;
							}else{
								$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-default").removeClass("badge-warning");
								totalPendingNotifCount--;
							}

							$('.top-menu .dropdown-notification').find(".dropdown-toggle > span").text(totalPendingNotifCount);

							angular.element(document.getElementById('quicksidebarController')).scope().updateAthletePendingNotice(this.objectId, this.tableName);
						}
                    }

					$("#toastrOptions").text("Command: toastr[" + shortCutFunction + "](\"" + msg + (title ? "\", \"" + title : '') + "\")\n\ntoastr.options = " + JSON.stringify(toastr.options, null, 2));

					if(strPedingNoticeIdnex.indexOf(index + "#") < 1){
						
						strPedingNoticeIdnex = strPedingNoticeIdnex + index + "#";
					}
					if(strPedingNoticeIdnex.indexOf(index + "#") < 1 || multiShow == 2){
						var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
						$toastlast = $toast;
					}
					
				});
			});
        }

    };

}();