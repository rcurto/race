(function (angular) {
    'use strict';
    angular.module('core').service('atletaMesocicleFactory', atletaMesocicleFactory);

    atletaMesocicleFactory.$inject = ['atletaMesocicleService'];

    function atletaMesocicleFactory(atletaMesocicleService) {
        return {
            //getById: getById,
            //getAll: getAll,
            //store: store,
            getTrainingForRange: getTrainingForRange

        };



        /*function store(data, callback) {
            var parseObject = Parse.Object.extend("atleta_mesocicle");
            var store = new parseObject();
            var i;
            for (i in data) {
                if (i == 'parseObject') {
                    continue;
                }
                store.set(i, data[i]);
            }
            store.save(null, {
                success: function (obj) {
                    callback({success: true});
                },
                error: function (obj, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getById(id, callback) {
            var parseObject = Parse.Object.extend("atleta_mesocicle");
            var query = new Parse.Query(parseObject);
            query.include('circuit');
            query.include('joined');
            query.get(id, {
                success: function (results) {
                    callback(cursesService(results))
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }*/

        function getTrainingForRange(date_start, date_end, user, callback) {
            console.log(date_start)
            var start = moment(date_start)
            console.log(start)

            start.subtract( start.format('E')-1, 'days');
            console.log(start)
            console.log(start.local().toDate())
            console.log(start.utc().toDate())
            console.log(start.utcOffset(0).toDate())
            console.log(start.utc(0).toDate())

            var end = moment(moment(date_end).format('YYYY-MM-DD'),'YYYY-MM-DD').utc()
            var dayOfNUmber = end.format('E');

            end.add( 7-dayOfNUmber, 'days');
            var parseObject = Parse.Object.extend("atleta_mesocicle");
            var query = new Parse.Query(parseObject);
            if ( user!=undefined ){
                query.equalTo('user', user);
            }
            query.greaterThanOrEqualTo('datini', start.utc(0).utcOffset(0).toDate() );
            query.lessThanOrEqualTo('datfi', end.toDate() );

            query.include('dijous');
            query.include('dijous.l1');
            query.include('dijous.l2');
            query.include('dijous.l3');
            query.include('dijous.l4');
            query.include('dijous.l5');
            query.include('dijous.l6');

            query.include('dilluns');
            query.include('dilluns.l1');
            query.include('dilluns.l2');
            query.include('dilluns.l3');
            query.include('dilluns.l4');
            query.include('dilluns.l5');
            query.include('dilluns.l6');

            query.include('dimarts');
            query.include('dimarts.l1');
            query.include('dimarts.l2');
            query.include('dimarts.l3');
            query.include('dimarts.l4');
            query.include('dimarts.l5');
            query.include('dimarts.l6');

            query.include('dimecres');
            query.include('dimecres.l1');
            query.include('dimecres.l2');
            query.include('dimecres.l3');
            query.include('dimecres.l4');
            query.include('dimecres.l5');
            query.include('dimecres.l6');

            query.include('dissabte');
            query.include('dissabte.l1');
            query.include('dissabte.l2');
            query.include('dissabte.l3');
            query.include('dissabte.l4');
            query.include('dissabte.l5');
            query.include('dissabte.l6');

            query.include('diumenge');
            query.include('diumenge.l1');
            query.include('diumenge.l2');
            query.include('diumenge.l3');
            query.include('diumenge.l4');
            query.include('diumenge.l5');
            query.include('diumenge.l6');

            query.include('divendres');
            query.include('divendres.l1');
            query.include('divendres.l2');
            query.include('divendres.l3');
            query.include('divendres.l4');
            query.include('divendres.l5');
            query.include('divendres.l6');

            query.ascending('datini');

            query.limit(1000);
            query.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push( atletaMesocicleService(row) );
                    });
                    callback(rows);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }
        /*
        function getAll(callback) {
            var parseObject = Parse.Object.extend("atleta_mesocicle");
            var query = new Parse.Query(parseObject);
            //query.greaterThan('data', new Date());
            //query.include('circuit');
            //query.include('joined');

            query.limit(1000);
            query.find({
                success: function (results) {
                    cache = results;
                    var events = [];
                    angular.forEach(results, function (row) {
                        var eventObject = cursesService(row);
                        events.push(eventObject);
                    });
                    callback(events);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }*/


    }
})(angular);

