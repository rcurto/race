(function (angular) {
    'use strict';
    angular.module('loginApp', [ 'core', 'gettext']).

    run(['userFactory', '$rootScope', 'gettextCatalog','$location', function (userFactory, $rootScope, gettextCatalog,$location) {
        //gettextCatalog.currentLanguage = 'french';

        gettextCatalog.debug = true;

        userFactory.auth(function (user) {
            if ( user!= null ){
                window.location = 'app.html';
            }
            gettextCatalog.setCurrentLanguage('english');
        })

    }]);

})(angular);


