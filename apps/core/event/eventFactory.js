(function (angular) {
    'use strict';
    angular.module('core').service('eventFactory', eventFactory);

    eventFactory.$inject = ['eventService','$filter'];

    function eventFactory(eventService, $filter) {
        var cache = [];
        return {
            getById: getById,
            getAll: getAll,
            joinRace: joinRace,
            getUnreadEvents: getUnreadEvents,
            getLastEvents: getLastEvents,
            getJoinedRaceEvents: getJoinedRaceEvents,
            createCoachFeedback: createCoachFeedback,
            createRaceFeedback: createRaceFeedback,
            createRaceTips: createRaceTips,
            createRequest: createRequest
        };

        function createRequest(user, request) {
            var parseObject = Parse.Object.extend("Event");
            var store = new parseObject();
            store.set('user', user);

            var message = $filter('translate')("Coach comment your %subject% request");
            message = message.replace('%subject%',  request.get("subject"));

            store.set('text', message );
            store.set('request', request );
            store.set('type', 'request' );
            store.save(null, {});
        }

        function createRaceTips(user, race) {
            var parseObject = Parse.Object.extend("Event");
            var store = new parseObject();
            store.set('user', user);

            var message = $filter('translate')("The race tips for the %race% are ready");
            message = message.replace('%race%',  race.get("nom"));

            store.set('text', message );
            store.set('curses', race );
            store.set('type', 'raceTip' );
            store.save(null, {});
        }

        function createRaceFeedback(user, race) {
            var parseObject = Parse.Object.extend("Event");
            var store = new parseObject();
            store.set('user', user);
            store.set('text', $filter('translate')("Coach comment your race feedback") );
            store.set('curses', race );
            store.set('type', 'raceFeedback' );
            store.save(null, {});
        }

        function createCoachFeedback(user) {
            var parseObject = Parse.Object.extend("Event");
            var store = new parseObject();
            store.set('user', user);
            store.set('text', $filter('translate')("Coach comment your weekly feedback") );
            store.set('type', 'weeklyFeedback' );
            store.save(null, {});
        }

        function getLastEvents(user, limit){
            console.log(limit)
            var promise = new Parse.Promise();
            var query = new Parse.Query( Parse.Object.extend("Event"));
            query.equalTo('user', user.parseObject);
            //query.equalTo('flgUserView', undefined);
            query.include('user');
            query.include('curses');
            query.descending('createdAt');
            query.limit(limit);
            query.find({
                success: function (results) {
                    var events = [];
                    angular.forEach(results, function (row) {
                        events.push(eventService(row));
                    });
                    promise.resolve(events);
                }
            });
            return promise;
        }

        function getUnreadEvents(user){
            var promise = new Parse.Promise();
            var query = new Parse.Query( Parse.Object.extend("Event"));
            query.equalTo('user', user.parseObject);
            query.equalTo('flgUserView', undefined);
            query.include('user');
            query.include('curses');
            query.descending('createdAt');
             query.find({
                success: function (results) {
                    var events = [];
                    angular.forEach(results, function (row) {
                        events.push(eventService(row));
                    });
                    promise.resolve(events);
                }
            });
            return promise;
        }

        function getJoinedRaceEvents(limit){
            var promise = new Parse.Promise();
            var query = new Parse.Query( Parse.Object.extend("Event"));
            //query.equalTo('user', user.parseObject);
            query.equalTo('type', null);
            query.include('user');
            query.include('curses');
            query.limit(limit);
            query.descending('createdAt');
             query.find({
                success: function (results) {
                    var events = [];
                    angular.forEach(results, function (row) {
                        events.push(eventService(row));
                    });
                    promise.resolve(events);
                }
            });
            return promise;
        }

        function joinRace(race, user) {
            var parseObject = Parse.Object.extend("Event");
            var store = new parseObject();
            store.set('user', user);
            store.set('curses', angular.copy(race) );
            var name = '';
            if ( user.get('firstName')!=undefined ){
                name+= user.get('firstName');
            }
            if ( user.get('lastName')!=undefined ){
                name+=' '+user.get('lastName');
            }
            store.set('text', name+' '+$filter('translate')('sign on')  + ' ' + race.get('nom'));
            store.save(null, {});
        }

        function getById(id, callback) {
            var parseObject = Parse.Object.extend("Event");
            var query = new Parse.Query(parseObject);
            query.include('user');
            query.include('curses');
            query.get(id, {
                success: function (results) {
                    callback(eventService(results))
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }


        function getAll(limit, callback) {
            var parseObject = Parse.Object.extend("Event");
            var query = new Parse.Query(parseObject);
            query.limit(limit);
            query.include('user');
            query.include('curses');
            query.descending('createdAt');
            query.find({
                success: function (results) {
                    cache = results;
                    var events = [];
                    angular.forEach(results, function (row) {
                        var eventObject = eventService(row);
                        events.push(eventObject);
                    });
                    callback(events);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }



    }
})(angular);

