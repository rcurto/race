(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('faqListController', faqListController);

    faqListController.$inject = ['$scope', '$sce', 'userFactory'];
    function faqListController($scope, $sce, userFactory) {

        $scope.env = {
            user         : null,
            sectionList  : ['general', 'mygoals', 'races', 'feedbacks', 'trainings', 'myraces', 'dashboard', 'tops', 'requests', 'racetips', 'chat'],
            faqList      : {},
            accordionList: {}
        };
        userFactory.auth(function (user) {

            $scope.env.user = user;

            var userLanguage = user.language;

            var objFaqQuestions = Parse.Object.extend("FAQ");
            var query = new Parse.Query(objFaqQuestions);

            query.include("quesObjId");
            query.ascending("section");

            query.find({
                success: function(results) {
                    var collapseIndex  = 0;
                    var accordionIndex = 0;

                    for(var i= 0; i< $scope.env.sectionList.length; i++){
                        
                        var tmpAry = new Array();
                        $scope.env.faqList[$scope.env.sectionList[i]] = {};

                        for (var j = 0, len = results.length; j < len; j++) {

                            var result = results[j];

                            if($scope.env.sectionList[i] == result.get("quesObjId").get("section")){

                                tmpAry.push({ques:result.get("quesObjId").get(userLanguage), answer : result.get(userLanguage), 
                                            collapse : "collapse_" + collapseIndex});

                                collapseIndex++;
                            }
                                
                        }

                        $scope.env.accordionList[$scope.env.sectionList[i]] = "accordion" + accordionIndex;
                        $scope.env.faqList[$scope.env.sectionList[i]] = tmpAry;
                        
                        accordionIndex++;
                    }
//console.log($scope.env.faqList);
                    $scope.$apply();
                },
                error: function(error) {
                    alert("Error: " + error.code + " " + error.message);
                } 
            });

            $scope.renderHtml = function(html_code){
//console.log(html_code);                
                return $sce.trustAsHtml(html_code);
            }
        });
    }
})(angular);