<?php

require_once dirname(__FILE__)."/src/phpfreechat.class.php";

$nickname = "";
$photourl = "";

/*
if(!isset($_GET["sessionservertocken"]) || !$_GET["sessionservertocken"]){
	exit;
}

if($_GET["sessionservertocken"] != "80fd8a93551b9cc5ddb79b6380962c138cffae15"){
	exit;
}
*/
if(isset($_GET["nickname"]))
	$nickname = $_GET["nickname"];
else
	$nickname = "";

if(isset($_GET["photo_url"])) {
	$photourl = $_GET["photo_url"];
	$imagepath = "data/private/images";
	if (!file_exists($imagepath)) @mkdir_r($imagepath);
	if (file_exists($imagepath) && is_writable($imagepath))
	{
		$imagefile = $imagepath."/".$nickname.".log";
		file_put_contents($imagefile, $photourl);;
	}
}
else
	$photourl = "themes/default/images/nophoto.png";


$params = array();

$params["title"]			= "Quick chat";
$params["nick"]				=  $nickname;		// setup the intitial nickname
$params["photourl"]			=  $photourl;		// setup the intitial nickname
$params['firstisadmin']		= true;						//$params["isadmin"]	= true; // makes everybody admin: do not use it on production servers ;)
$params["serverid"]			= md5(__FILE__);			// calculate a unique id for this chat
$params["debug"] = false;

$chat = new phpFreeChat( $params );

?>
<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/generic.css" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/header.css" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/footer.css" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/menu.css" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/content.css" />  

  <!--<link rel="stylesheet" title="classic" type="text/css" href="style/components.css" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/plugins.css"/>
  <link rel="stylesheet" title="classic" type="text/css" href="style/layout.css" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/darkblue.css" />
  <link rel="stylesheet" title="classic" type="text/css" href="style/custom.css" />-->

  <link rel="stylesheet" title="classic" type="text/css" href="style/styles.css" />
  
  <link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
  <link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
  <link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
  <link href="../../assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
  <link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

  
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
  
  <!--<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>-->
 </head>
 <body style="width:100%; height:100%">
<div align="left" class="chats">
  <?php $chat->printChat(); ?>
</div>
</body></html>
