(function (angular) {
    'use strict';

    angular.module('mainApp').
    controller('usersListController', usersListController);

    usersListController.$inject = ['$scope', 'userFactory', '$filter'];
    function usersListController($scope, userFactory, $filter) {

        $scope.env = {
            loading: true,
            posts: [],
            tableConfig: {
                itemsPerPage: 50,
                fillLastPage: false
            },
            user: null,
            filters: {
                name: undefined,
                email: undefined,
                role: undefined
            }
        };

        $scope.remove = function remove(user ,index) {
            $scope.env.usersSource = $scope.env.usersSource.filter(function (row) {
                if (row.getId() != user.getId()) {
                    return true;
                } else {
                    return false;
                }
            });
            $scope.env.users.splice(index,1);
            user.delete();
        };



        userFactory.getAllUsers(function (answer) {
            $scope.env.loading = false;
            $scope.env.posts = answer;
            $scope.$apply()
        });

        function filter(filters, users) {
            if (users == undefined) {
                return false;
            }
            return $filter("filter")(users, filters);
        }

        $scope.$watchCollection('env.filters', function (value) {
            $scope.env.users = filter(value, $scope.env.usersSource)
        }, true)


    }
})(angular);