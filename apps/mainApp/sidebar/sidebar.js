(function (angular) {
    'use strict';

    function sidebarDirective() {
        return {
            replace:true,
            restrict: 'E',
            controller:headerMenuController,
            templateUrl: 'apps/mainApp/sidebar/sidebar.html',
            scope:{}
        };

        headerMenuController.$inject = ['$scope'];
        function headerMenuController($scope){

            $scope.env={
                showLogin: false,
            };

            $scope.$watch('$parent.user', function(user){
                if ( user ){
                    $scope.user = $scope.$parent.user;
                }
            })
        }

    }

    angular.module('MetronicApp').directive('sidebar', sidebarDirective);


})(window.angular);
