(function (angular) {
    'use strict';
    angular.module('core').service('circuitsFactory', circuitsFactory);

    circuitsFactory.$inject = ['circuitsService'];

    function circuitsFactory(circuitsService) {
        return {
            getById: getById,
            getAll: getAll,
            store: store
        };


        function store(data, callback) {
            var parseObject = Parse.Object.extend("Circuits");
            var store = new parseObject();
            var i;
            for (i in data) {
                if (i == 'parseObject') {
                    continue;
                }
                store.set(i, data[i]);
            }


            store.save(null, {
                success: function (obj) {
                    callback({success: true});
                },
                error: function (obj, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getById(id, callback) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("Circuits");
            var query = new Parse.Query(parseObject);
            query.get(id, {
                success: function (results) {
                    if (callback){
                        callback(circuitsService(results))
                    }
                    promise.resolve(circuitsService(results));
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;
        }


        function getAll(callback) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("Circuits");
            var query = new Parse.Query(parseObject);
            query.ascending('nom');
            query.descending('order');
            query.limit(1000);
            query.find({
                success: function (results) {
                    var objs = [];
                    angular.forEach(results, function (row) {
                        var eventObject = circuitsService(row);
                        objs.push(eventObject);
                    });
                    if (callback){
                        callback(objs);
                    }

                    promise.resolve(objs);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;
        }


    }
})(angular);

